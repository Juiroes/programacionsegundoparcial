﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraOfertas
{
    public partial class FormCalculadora : Form
    {
        CSueldo c = new CSueldo();
        public FormCalculadora()
        {
            InitializeComponent();
            txtUMA.Text = Convert.ToString(c.getUMA());
        }

        

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            c.setSalario( double.Parse(txtSueldoDiario.Text));
            c.setDias( double.Parse(txtDiasLaborados.Text));

            txtSueldoDiarioIntegrado.Text = Convert.ToString(c.encontrarSDI());
            txtISR.Text = Convert.ToString(c.ISR());
            txtSubsidio.Text =  Convert.ToString(c.Subsidio());
            txtCuotaIMSS.Text = Convert.ToString(c.encontrarIMSS());
            double deducciones =  c.ISR() - c.Subsidio() + c.encontrarIMSS();
            double percepciones = c.getSueldo();
            txtTotalDeducciones.Text = Convert.ToString(deducciones);
            txtTotalPer.Text = Convert.ToString(percepciones);
            txtSueldosSalarios.Text = Convert.ToString(percepciones);
            double sueltoNeto = percepciones - deducciones;
            if (sueltoNeto > 0)
            {
                txtSueldoNeto.Text = Convert.ToString(sueltoNeto);
            }
            else
            {
                txtSueldoNeto.Text =" 0 ";
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
