﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Carreras
{
    class CTask
    {
        Task[] tareas;
        int[] id;
        int b;

        public CTask(int a)
        {
            tareas = new Task[a];
            id = new int[a];
            b = a;
        }

      

        public  async Task<bool>  hacerRandom()
        {
            Random r = new Random();
            Action<object> action = (object obj) =>
            {
                Console.WriteLine(" Corredor {0} listo para competir ",
               Task.CurrentId);
            };

            for (int i = 0; i < tareas.Length; i++)
            {
                tareas[i] = new Task(action, new string('1', i + 1));                
                id[i] = r.Next(1 , (b*2));
                tareas[i].Start();
            }

            

            await  Task.WhenAll(tareas);    
            return true;
        }

        public async void proceso()
        {
            bool r = await hacerRandom();
            var source1 = new CancellationTokenSource();
            var token1 = source1.Token;
            Console.WriteLine();
            if (r)
            {
                for (int i = 0; i < tareas.Length; i++)
                {

                    if (id[i] > (b*.1) && id[i] < (b*.25))
                    { 
                        Console.WriteLine("Este es el case 0, el carro {0} ha chocado", tareas[i].Id);
                        tareas[i] = Task.Run(() => Thread.Sleep(1),
                          token1);
                    }
                    else if(id[i] > (b*.26) && id[i]< (b*.50))
                    {
                        Console.WriteLine("Este es el case 1, el carro {0} se descompuso", tareas[i].Id);
                        tareas[i] = Task.Run(() => Thread.Sleep(1),
                         token1);
                    }
                    else
                    {
                        Console.WriteLine("El case default, el carro {0} sigue corriendo", tareas[i].Id);
                    }
                       
                  
                    
                }
            }
        }

    }
}
