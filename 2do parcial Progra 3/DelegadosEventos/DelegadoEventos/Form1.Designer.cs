﻿namespace DelegadoEventos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClick = new System.Windows.Forms.Button();
            this.lblUno = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClick
            // 
            this.btnClick.Location = new System.Drawing.Point(26, 96);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(136, 38);
            this.btnClick.TabIndex = 0;
            this.btnClick.Text = "Dale click";
            this.btnClick.UseVisualStyleBackColor = true;
            // 
            // lblUno
            // 
            this.lblUno.AutoSize = true;
            this.lblUno.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.lblUno.Location = new System.Drawing.Point(33, 41);
            this.lblUno.Name = "lblUno";
            this.lblUno.Size = new System.Drawing.Size(54, 13);
            this.lblUno.TabIndex = 1;
            this.lblUno.Text = "Dale click";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(211, 310);
            this.Controls.Add(this.lblUno);
            this.Controls.Add(this.btnClick);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.Label lblUno;
    }
}

