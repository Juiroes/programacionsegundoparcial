﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
    class CTrabajo
    {
        private string nombre;
        private string edad;
        private string sexo;
        
        public CTrabajo(string pNombre, string pEdad, string pSexo)
        {
            nombre = pNombre;
            edad = pEdad;
            sexo = pSexo;
        }

        public static void mostrarPersonas(List<CTrabajo>  c)
        {
          
          foreach(CTrabajo job in c)
            {
                Console.WriteLine("El nombre  es {0}, la edad es {1} y su sexo es {2} ",job.nombre, job.edad,job.sexo);
            }

            Console.ReadLine();
         }
    }
}
