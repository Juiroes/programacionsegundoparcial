﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegados
{
   
    class Program
    {
        static void Main(string[] args)
        {
            CTrabajo personaUno = new CTrabajo("Isaac", "19", "Masculino");
            CTrabajo personaDos = new CTrabajo("Jose", "17", "Masculino");
            CTrabajo personaTres = new CTrabajo("Maria", "52", "Femenino");
            CTrabajo personaCuatro = new CTrabajo("Hermilo", "51", "Masculino");

            List<CTrabajo> list = new List<CTrabajo>();

            list.AddRange(new CTrabajo[] { personaUno, personaDos, personaTres, personaCuatro });

            mostrar m = new mostrar(CTrabajo.mostrarPersonas);

            m(list);



        }
        public delegate void mostrar(List<CTrabajo> d);

    }
}
