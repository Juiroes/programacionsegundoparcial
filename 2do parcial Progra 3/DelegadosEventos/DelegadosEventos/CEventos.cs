﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegadosEventos
{
    class CEventos
    {
        public delegate void Resultado(int valor);
        public event Resultado CalculoFinalizado;

        public void Calcular()
        {
            var evento = CalculoFinalizado;
            if(evento!=null)
            {
                evento(20);
            }
        }
    }
}
