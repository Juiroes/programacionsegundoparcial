﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegadosEventos
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculo = new CEventos();

            calculo.CalculoFinalizado += calculo_CalculoFinalizado;
            Console.WriteLine("Primer Paso");

            calculo.Calcular();

            Console.ReadLine();


        }

        static void calculo_CalculoFinalizado(int resultado)
        {
            Console.WriteLine(resultado);
        }
    }
}
