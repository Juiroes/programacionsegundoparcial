﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsEjemploCuatro
{
   
    class Program
    {
        static Random rand = new Random();

        static void Main()
        {
            // Wait on a single task with no timeout specified.
            Task taskA = Task.Run(() => Thread.Sleep(5000));
            Console.WriteLine("taskA Status: {0}", taskA.Status);
            try
            {
                taskA.Wait();
                Console.WriteLine("taskA Status: {0}", taskA.Status);
            }
            catch (AggregateException)
            {
                Console.WriteLine("Exception in taskA.");
            }
            Console.ReadLine();
        }
    }
    // The example displays output like the following:
    //     taskA Status: WaitingToRun
    //     taskA Status: RanToCompletion
}
