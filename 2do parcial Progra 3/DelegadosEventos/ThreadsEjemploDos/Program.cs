﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadsEjemploDos
{
    public class Example
    {
        public static async Task Main()
        {
            await Task.Run(() => {
                // Just loop.
                int ctr = 0;
                for (ctr = 0; ctr <= 1000000; ctr++)
                { }
                Console.WriteLine("Finished {0} loop iterations",
                                  ctr);
            });
            Console.ReadLine();
        }
    }
    // The example displays the following output:
    //        Finished 1000001 loop iterations

}
